# LeetCode Notes


### Intervals


- 23. meeting rooms
- 56. merge intervals
- 435. non-overlapping intervals


### String parsing


- 880. Decoded String at Index

### Dynamic programming

- 42. Trapping Rain Water
- 1143. Longest Common Subsequence
- 403. Frog Jump
- 1235. Maximum Profit in Job Scheduling 

### Stacks

- 1172. Diner plate stacks (Stack with TreeSet)
- 402. Remove K digits
- 735. Asteroid Collision
- 316. Remove Duplicate Letters

### Queue

- 239. Sliding Window Maximum

### Recursion

- 395. Longest Substring with At Least K Repeating Characters

### Backtracking 

- 465. Optimal Account Balancing
- 320. Generalized Abbreviation
- 698. Partition to K Equal Sum Subsets

### Binary Search

- 1283. Find the Smallest Divisor Given a Threshold

### Heap/Priority Queue

- 407. Trapping Rain Water II 

### Tricks (No Category)

These questions are difficult to categorize and often needs more specific analysis that does not have a common application. 
- 406. Queue Reconstruction by Height
- 777. Swap Adjacent in LR String

### Trees

In the problems related to trees, the most common techiniques are `recursion` and `backtracking`
- 1339. Maximum Product of Splitted Binary Tree
- 449. Serialize and Deserialize BST
- 440. K-th Smallest in Lexicographical Order
- 386. Lexicographical Numbers 

### Prefix Sums

- 974. Subarray Sums divisible by K

### Merge Sort

User tricks similar to merge sort to solve problems

- 493. Reverse Pairs (Binary Search Tree)

### DFS/BFS

- 351. Android Unlock Patterns
- 329. Longest Increasing Path in a Matrix (memorization, not DP yet)
- 1192. Critical Connection in a Network


### Map

- 146. LRU Cache (Doubly Linked List)

### List 

- 25. Reverse Nodes in k-Group 

### Graph

- 329. Longest Increasing Path in a Matrix (Topological Sort)